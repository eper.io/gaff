# gaff billing example

## Getting started

Gaff is a sample billing solution that uses Golang UI streamed remotely with PNG streaming.

## Design considerations

It is an example solution to sell random numbers.
Why would you need such merchandise??

It is security. Malicious actors can try to tamper with random number generators to spy on encrypted traffic.
Crypto algorithms, coin solution all rely on reliably random numbers.

Software usually uses locally generated cryptographic random numbers.
These numbers are cryptographic grade vs. pseudo random numbers,
if they do not have any dependency between them.

One way to achieve such entropy is to do a bitwise xor function on independently sourced random numbers.
You may download such numbers with an encrypted channel from other sites making your number stronger.

How much? If you have 1% probability of tampering, an attacker has to tamper with all the sources.
There is a 99.9999% probability of randomness assuming three completely independent sources.

```
VALUE=1-0.01^3
```

This is an example solution where vendors can buy such random sources,
so that they can use these numbers as seeds for their own random number
generators. They can basically 'mine' random numbers that others can use
to secure their systems better.

The ideal local generator generates a random flag whether to use such
an imported entropy or not in the local generator algorithm.
Malicious local malware can be caught measuring the prevalence of such a flag.

## How to use

Download the sources
```
git clone https://gitlab.com/eper.io/gaff.git
```

Update the project parameters with your own data once you tried it in `metadata/data.go`

Run the project
```
go run ./main.go
```

Check the site and log in with the activation key found in `metadata/data.go`
```
http://127.0.0.1:7777/
```

Bookmark the management url that shows up. This is your secret to get site logs for example: `ICSABAJFFLDPPFEDPGDHETTBTSJGLEOPSJTNHCRLJGANIFFIGCHFHADGDIDFDNMS`
```
http://127.0.0.1:7777/management.html?apikey=ICSABAJFFLDPPFEDPGDHETTBTSJGLEOPSJTNHCRLJGANIFFIGCHFHADGDIDFDNMS
```

You can give the order link to your clients.
```
http://127.0.0.1:7777/checkout.html?apikey=THGABTGADSSIEOGSEKAEMHDCJEABLBEPASTRROCRDGQAGJQISBTSTQFPAKRANTKA
```

Make sure you use the load balancer with TLS of your cloud provider.
The final URL should look like:
```
https://yourdomain.example.com/photos.html?apikey=THGABTGADSSIEOGSEKAEMHDCJEABLBEPASTRROCRDGQAGJQISBTSTQFPAKRANTKA
```

## Docker images

Here are some docker images to play with
```
docker pull registry.gitlab.com/eper.io/gaff:latest
docker pull eperio/gaff
```

## License

This document is Licensed under Creative Commons CC0. To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this document to the public domain worldwide. This document is distributed without any warranty. You should have received a copy of the CC0 Public Domain Dedication along with this document. If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode>.  